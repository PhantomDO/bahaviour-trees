﻿using BTBolt.Tools;
using System;
using UnityEngine;

namespace BTBolt.Actions
{
    [System.Serializable]
    public class BTAttack : BTAction<BTAttack>, INode
    {
        public Transform Target = null;
        
        public BTAttack(string name = null) : base(name) { }

        public override BTNodeState OnEvaluate()
        {
            return BTNodeState.FAILURE;
        }
    }
}
