﻿
using Bolt;
using System;

namespace BTBolt.Actions
{
    [System.Serializable]
    [UnitCategory("BehaviorTree/Actions")]
    public abstract class BTAction<T> : BTNode<T>, INode
    {
        protected BTAction(string name = null) : base(name) { }
    }
}
