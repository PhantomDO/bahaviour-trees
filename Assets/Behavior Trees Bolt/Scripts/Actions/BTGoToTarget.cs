﻿using BTBolt.Tools;
using System;
using UnityEngine;

namespace BTBolt.Actions
{
    [System.Serializable]
    public class BTGoToTarget : BTAction<BTGoToTarget>, INode
    {
        public Transform Target = null;
        
        public BTGoToTarget(string name = null) : base(name) { }

        public override BTNodeState OnEvaluate()
        {
            return BTNodeState.FAILURE;
        }
    }
}
