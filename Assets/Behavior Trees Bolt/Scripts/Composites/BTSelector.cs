﻿using Ludiq;
using System.Collections.Generic;
using UnityEngine;

namespace BTBolt.Composites
{
    [System.Serializable]
    public class BTSelector : BTComposite<BTSelector>, INode
    {
        public BTSelector(string name = null) : base(name) { }

        public override BTNodeState OnEvaluate()
        {
            foreach (var node in this.ConnectedNodes)
            {
                Debug.LogWarning($"{Name} evaluate[{ConnectedNodes.IndexOf(node)}] : {node.Name}");
                switch (node.OnEvaluate())
                {
                        case BTNodeState.RUNNING:
                            NodeState = BTNodeState.RUNNING;
                            return NodeState;
                        case BTNodeState.SUCCESS:
                            NodeState = BTNodeState.SUCCESS;
                            return NodeState;
                        case BTNodeState.FAILURE:
                            break;
                        default:
                            break;
                    
                }
            }

            NodeState = BTNodeState.FAILURE;
            return NodeState;
        }
    }
}
