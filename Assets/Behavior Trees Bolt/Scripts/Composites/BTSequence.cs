﻿using Ludiq;
using System.Collections.Generic;
using UnityEngine;

namespace BTBolt.Composites
{
    [System.Serializable]
    public class BTSequence : BTComposite<BTSequence>, INode
    {
        public BTSequence(string name = null) : base(name) { }

        public override BTNodeState OnEvaluate()
        {
            bool isAnyNodeRunning = false;

            foreach (var node in this.ConnectedNodes)
            {
                Debug.LogWarning($"{Name} evaluate[{ConnectedNodes.IndexOf(node)}] : {node.Name}");
                switch (node.OnEvaluate())
                {
                        case BTNodeState.RUNNING:
                            isAnyNodeRunning = true;
                            break;
                        case BTNodeState.SUCCESS:
                            break;
                        case BTNodeState.FAILURE:
                            NodeState = BTNodeState.FAILURE;
                            return NodeState;
                        default:
                            break;
                    
                }
            }

            // If Success all the node in the list was a success
            NodeState = isAnyNodeRunning ? BTNodeState.RUNNING : BTNodeState.SUCCESS;
            return NodeState;
        }
    }
}
