﻿
using BTBolt.Tools;
using Ludiq;
using Bolt;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections;

namespace BTBolt.Composites
{
    [System.Serializable]
    public abstract class BTComposite<T> : BTNode<T>, INode
    {
        protected BTComposite(string name = null) : base(name) { }

        public override void SetConnectedNode(INode node)
        {
            if (ConnectedNodes.Contains(node))
                ConnectedNodes.RemoveAt(ConnectedNodes.IndexOf(node));
            
            if (!ConnectedNodes.Contains(node))
                ConnectedNodes.Add(node);
        }
    }
}
