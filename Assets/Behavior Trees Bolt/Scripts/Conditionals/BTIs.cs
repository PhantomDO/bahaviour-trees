﻿
namespace BTBolt.Conditionals
{
    [System.Serializable]
    public class BTIs : BTConditional<BTHas>, INode
    {
        public BTIs(string name = null) : base(name) { }

        public override BTNodeState OnEvaluate()
        {
            return NodeState;
        }
    }
}
