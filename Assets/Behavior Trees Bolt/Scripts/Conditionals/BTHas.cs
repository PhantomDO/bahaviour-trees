﻿
namespace BTBolt.Conditionals
{
    public enum HasType { S, F}

    [System.Serializable]
    public class BTHas : BTConditional<BTHas>, INode
    {
        public HasType Type = HasType.F;

        public BTHas(string name = null) : base(name) { }

        public override BTNodeState OnEvaluate()
        {
            switch (Type)
            {
                case HasType.F:
                    NodeState = BTNodeState.FAILURE;
                    break;
                case HasType.S:
                    NodeState = BTNodeState.SUCCESS;
                    break;
                default:
                    break;
            }

            return NodeState;
        }
    }
}
