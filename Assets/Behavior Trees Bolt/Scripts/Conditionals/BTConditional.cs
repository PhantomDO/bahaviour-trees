﻿
using Bolt;
using System;

namespace BTBolt.Conditionals
{
    [System.Serializable]
    [UnitCategory("BehaviorTree/Conditionals")]
    public abstract class BTConditional<T> : BTNode<T>, INode
    {
        protected BTConditional(string name = null) : base(name) { } 
    }
}
