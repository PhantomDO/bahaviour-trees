﻿
using System;
using System.Collections.Generic;

namespace BTBolt
{
    public interface INode
    {
        BTNodeState NodeState { get; }
        Type BaseType { get; }
        Type RootType { get; }
        string Name { get; set; }
        INode StartNode { get; set; }
        List<INode> ConnectedNodes { get; set; }
        void SetConnectedNode(INode node);

        void OnInit();
        BTNodeState OnEvaluate();
    }
}
