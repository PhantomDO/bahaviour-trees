﻿
using Bolt;
using System;

namespace BTBolt.Decorators
{
    [System.Serializable]
    [UnitCategory("BehaviorTree/Decorators")]
    public abstract class BTDecorator<T> : BTNode<T>, INode
    {
        public INode ConnectedNode => (ConnectedNodes.Count > 0) ? ConnectedNodes[0] : null;

        public override void SetConnectedNode(INode node)
        {
            if (ConnectedNodes.Count >= 1 || ConnectedNodes.Contains(node))
                ConnectedNodes.RemoveAt(0);

            if (ConnectedNodes.Count < 1 && !ConnectedNodes.Contains(node))
                ConnectedNodes.Add(node);
        }

        protected BTDecorator(string name = null) : base(name) { }
    }
}
