﻿
using UnityEngine;

namespace BTBolt.Decorators
{
    [System.Serializable]
    public class BTInverter : BTDecorator<BTInverter>, INode
    {
        public BTInverter(string name = null) : base(name) { }

        public override BTNodeState OnEvaluate()
        {
            Debug.Log($"{Name} evaluate[0] : {ConnectedNode.Name}");
            switch (ConnectedNode.OnEvaluate())
            {
                case BTNodeState.RUNNING:
                    NodeState = BTNodeState.RUNNING;
                    break;
                case BTNodeState.SUCCESS:
                    NodeState = BTNodeState.FAILURE;
                    break;
                case BTNodeState.FAILURE:
                    NodeState = BTNodeState.SUCCESS;
                    break;
                default:
                    break;
            }

            return NodeState;
        }
    }
}
