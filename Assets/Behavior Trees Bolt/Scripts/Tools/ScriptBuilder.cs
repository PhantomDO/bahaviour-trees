﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using UnityEditor.Compilation;
using UnityEngine;
using UnityEditor;

namespace BTBolt.Tools
{
    public class ScriptBuilder
    {
        [UnityEditor.MenuItem("Tools/Debug Assemblies")]
        private static void DebugAssemblies()
        {
            var assems = AppDomain.CurrentDomain.GetAssemblies();
            string str = $"List of assemblies in the current domain : \n";
            foreach (var a in assems)
                str += $"\t- {a.ToString()}\n";

            Debug.Log(str);
        }

        public static System.Reflection.Assembly GetAssembly(string assemblyName)
        {
            var assems = AppDomain.CurrentDomain.GetAssemblies();
            var result = assems.Where(a => a.GetName().Name.Contains(assemblyName));
            return result.FirstOrDefault();
        }

        private static void BuildAssembly(bool wait, string tempDirectory, string directory, string assemblyName)
        {
            var outputAssembly = tempDirectory + assemblyName;
            var assemblyProjectPath = directory + assemblyName;
            var scripts = new List<string>();

            var headerNamespace = "BTBolt.";
            var parentType = new[] { "Actions", "Conditionals", "Composites", "Decorators" };

            System.IO.Directory.CreateDirectory(tempDirectory.Substring(0, tempDirectory.Length - 1));
            System.IO.Directory.CreateDirectory(directory.Substring(0, directory.Length - 1));

            for (int i = 0; i < parentType.Length; i++)
            {
                GenerateScripts(tempDirectory, parentType[i], headerNamespace, ref scripts);
            }

            var assemblyBuilder = new AssemblyBuilder(outputAssembly + ".dll", scripts.ToArray());

            assemblyBuilder.excludeReferences = new string[] { assemblyProjectPath + ".dll" };

            assemblyBuilder.buildStarted += delegate (string assemblyPath)
            {
                Debug.LogFormat("Assembly build started for {0}", assemblyPath);
            };

            assemblyBuilder.buildFinished += delegate (string assemblyPath, CompilerMessage[] compilerMessages)
            {
                var errorCount = compilerMessages.Count(m => m.type == CompilerMessageType.Error);
                var warningCount = compilerMessages.Count(m => m.type == CompilerMessageType.Warning);

                Debug.LogFormat("Assembly build finished for {0}", assemblyPath);
                Debug.LogFormat("Warnings: {0} - Errors: {0}", warningCount, errorCount);

                if (errorCount == 0)
                {
                    File.Copy(outputAssembly + ".dll", assemblyProjectPath + ".dll", true);
                    File.Copy(outputAssembly + ".pdb", assemblyProjectPath + ".pdb", true);
                    var text = "TARGET_INCLUDE_ALL;TARGET_BACKEND_IL2CPP;RELEASE;NET_4_6";
                    File.WriteAllText(assemblyProjectPath + ".defines", text);
                    AssetDatabase.ImportAsset(assemblyProjectPath + ".dll");
                    AssetDatabase.ImportAsset(assemblyProjectPath + ".pdb");
                }
            };

            if (!assemblyBuilder.Build())
            {
                Debug.LogErrorFormat("Failed to start build of assembly {0}!", assemblyBuilder.assemblyPath);
                return;
            }

            if (wait)
            {
                while (assemblyBuilder.status != AssemblyBuilderStatus.Finished)
                    System.Threading.Thread.Sleep(10);
            }
        }

        private static void GenerateScripts(string directory, string folder, string headerNamespace, ref List<string> scripts)
        {
            System.IO.Directory.CreateDirectory(directory + folder);
            var types = typeof(ScriptBuilder).Assembly.GetStrTypesFromNamespace(headerNamespace + folder);
            var ptr = new Dictionary<string, string>();

            foreach (var type in types)
            {
                var typeSub = type.Substring(2, type.Length - 2);
                var derivedTypeSub = folder.Substring(0, folder.Length - 1);

                if (!typeSub.Contains(derivedTypeSub) && type.Contains("BT"))
                {
                    var script = $"{typeSub}.cs";
                    var scriptText = $"using BTBolt;\nusing {headerNamespace + folder};\n";

                    ptr.Add("folder", folder);
                    ptr.Add("typeSub", typeSub);
                    ptr.Add("derivedTypeSub", derivedTypeSub);

                    if (directory.Contains("Factories"))
                    {
                        ptr.Add("pref", "Factory");
                        ptr.Add("namespace", headerNamespace + "Factories." + folder);
                        scriptText += FactoryTemplate(type, ref ptr);
                    }
                    else if (directory.Contains("Units") && !typeSub.Contains("Root"))
                    {
                        ptr.Add("pref", "Node");
                        ptr.Add("namespace", headerNamespace + "Units." + folder);
                        scriptText += NodeTemplate(type, ref ptr);
                    }

                    if (ptr.ContainsKey("pref"))
                    {
                        var scriptPath = directory + folder + '/' + ptr["pref"] + script;
                        File.WriteAllText(scriptPath, scriptText);
                        scripts.Add(scriptPath);

                        Debug.Log(scriptPath + " has been generated.");
                    }

                    ptr.Clear();
                }
            }
        }

        #region NODE

        [UnityEditor.MenuItem("Tools/Generate Bolt Units/Build Assembly Async")]
        public static void BuildUnitsAssemblyAsync()
        {
            BuildAssembly(false,
                "Assets/Behavior Trees Bolt/Scripts/Units/Generated/",
                "Temp/BTBoltUnitsAssembly/",
                "BTBoltUnitsAssembly");
        }

        [UnityEditor.MenuItem("Tools/Generate Bolt Units/Build Assembly Sync")]
        public static void BuildUnitsAssemblySync()
        {
            BuildAssembly(true,
                "Assets/Behavior Trees Bolt/Scripts/Units/Generated/",
                "Temp/BTBoltUnitsAssembly/",
                "BTBoltUnitsAssembly");
        }

        private static string NodeTemplate(string type, ref Dictionary<string, string> ptr)
        {
            var unitCategory = $"[UnitCategory(\"BehaviorTree/Node/{ ptr["folder"] }\")]";

            return ($"using Bolt;\nusing BTBolt.Units;\n\n" +
                $"namespace { ptr["namespace"] }\n" + "{\n" +
                $"\t{ unitCategory }\n\tpublic class { ptr["pref"] + ptr["typeSub"] } : " +
                $"{ ptr["pref"] + ptr["derivedTypeSub"] }<{ type }> " + " { }\n}");
        }

        #endregion

        #region FACTORY

        [UnityEditor.MenuItem("Tools/Generate Factory Type/Check Factories")]
        public static void CheckFactories() => BTNode<INode>.InitFactories().DebugFactories();

        [UnityEditor.MenuItem("Tools/Generate Factory Type/Build Assembly Async")]
        public static void BuildFactoriesAssemblyAsync()
        {
            BuildAssembly(false,
                "Temp/BTBoltFactoriesAssembly/",
                "Assets/Behavior Trees Bolt/Scripts/Factories/Generated/",
                "BTBoltFactoriesAssembly");
        }

        [UnityEditor.MenuItem("Tools/Generate Factory Type/Build Assembly Sync")]
        public static void BuildFactoriesAssemblySync()
        {
            BuildAssembly(true,
                "Temp/BTBoltFactoriesAssembly/",
                "Assets/Behavior Trees Bolt/Scripts/Factories/Generated/",
                "BTBoltFactoriesAssembly");
        }

        private static string FactoryTemplate(string type, ref Dictionary<string, string> ptr)
        {
            return ("\n" +
                $"namespace { ptr["namespace"] }\n" + "{\n" +
                $"\tpublic class { ptr["pref"] + ptr["typeSub"] } : NodeFactory" + "\n\t{\n" +
                $"\t\tpublic override INode Create(string name) => new { type }(name);" + "\n\t}\n}");
        }

        #endregion
    }
}
