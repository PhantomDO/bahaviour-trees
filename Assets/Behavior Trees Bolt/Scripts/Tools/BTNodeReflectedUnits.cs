﻿using Ludiq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace BTBolt.Tools
{
    [IncludeInSettings(true)]
    public static class BTNodeReflectedUnits
    {
        public static T To<T>(this INode node) => BTNodeTools.IsTypeOf<T>(node) ? (T)(object)node : (T)(object)null;

        public static INode Node(INode sourceNode, string newNode = "Root", string name = null)
        {
            if (sourceNode != null && name != null && name == sourceNode.Name)
                return sourceNode;

            if (sourceNode == null)
                return BTNodeTools.CreateNode(newNode, name);

            if (sourceNode.ConnectedNodes.Count <= 0 || sourceNode.ConnectedNodes == null)
                return BTNodeTools.AddToGraph(sourceNode, newNode, name);

            var result = sourceNode.ConnectedNodes.Where((INode n) => n.Name == name).ToList();
            Debug.Log($"Result count : {result.Count} = {(result.Count > 0 ? result[0].Name : "null")}");
            return (result.Count > 0) ? result.FirstOrDefault() : BTNodeTools.AddToGraph(sourceNode, newNode, name);
        }

        public static void SetFields<T>(this T sourceNode, Dictionary<string, object> fields)
        {
            foreach(var field in fields) 
                sourceNode.SetField(field.Key, field.Value);
        }

        public static void SetField<T>(this T sourceNode, string fieldName, object value)
        {
            var field = sourceNode.GetType().GetField(fieldName);
            if (field != null) field.SetValue(sourceNode, value);
        }

        public static IEnumerable<Type> GetTypesFromNamespace(this Assembly assembly, string desiredNamespace)
        {
            return assembly.GetTypes().Where(type => type.Namespace == desiredNamespace);
        }
        
        public static List<string> GetStrTypesFromNamespace(this Assembly assembly, string desiredNamespace)
        {
            var strTypes = new List<string>();
            var types = GetTypesFromNamespace(assembly, desiredNamespace);
            foreach(var type in types) strTypes.Add(type.Name);            
            return strTypes;
        }
    }
}
