﻿using BTBolt.Composites;
using BTBolt.Decorators;
using Ludiq;
using System;
using UnityEngine;

namespace BTBolt.Tools
{
    [IncludeInSettings(true)]
    public static class BTNodeTools
    {
        #region isRoot
        public static bool IsRootTypeOf(INode node, string type)
        {
            return node.RootType.Name.Contains(type);
        }
        #endregion

        #region isBase
        public static bool IsBaseTypeOf(INode node, string type)
        {
            return node.BaseType.Name.Contains(type);
        }
        #endregion

        #region isType
        public static bool IsTypeOf<T>(INode node)
        {
            return (typeof(T).Name == node.GetType().Name);
        }

        public static bool IsTypeOf(INode node, Type type)
        {
            return (type.Name == node.GetType().Name);
        }

        public static bool IsTypeOf(INode node, string type)
        {
            return (type == node.GetType().Name);
        }

        public static bool IsTypeOf<T>(Type type)
        {
            return (typeof(T).Name == type.Name);
        }
        #endregion

        #region Graph Utils

        public static void DoUpdate(INode node)
        {
            string debug = "";
            if (node.ConnectedNodes != null) debug = $"{node.Name} as {node.ConnectedNodes.Count} connected node."; 
            else debug = $"{node.Name} as {0} connected node.";
            Debug.Log(debug);

            node.OnEvaluate();

            if (node.NodeState == BTNodeState.FAILURE)
            {
                Debug.LogError($"{node.Name}.NodeState = {node.NodeState}");
            }
        }

        #endregion

        #region Node Utils

        public static INode DebugNode(INode node, Action<object> log, string message)
        {
            log($"{node.Name}" + message);
            return node;
        }

        public static INode CreateNode(string type, string name = null) 
            => DebugNode(BTNode<INode>.InitFactories().ExecuteCreation(type, name), Debug.LogWarning, " as been created.");

        public static INode AddChild(INode sourceNode, string type, string name = null)
        {
            var toAdd = CreateNode(type, name);
            sourceNode.SetConnectedNode(toAdd);
            return toAdd;
        }

        public static INode AddToGraph(INode sourceNode, string toAdd, string name = null)
        {
            INode added = null;

            if (IsBaseTypeOf(sourceNode, "BTDecorator") || IsBaseTypeOf(sourceNode, "BTComposite"))
                added = AddChild(sourceNode, toAdd, name);
            else
                added = CreateNode(toAdd, name);

            added.StartNode = sourceNode;

            return DebugNode(added, Debug.LogWarning, $" as been added to {sourceNode.Name}.");
        }

        #endregion
    }
}
