﻿using BTBolt.Composites;
using BTBolt.Actions;
using BTBolt.Conditionals;
using BTBolt.Decorators;
using UnityEngine;

namespace BTBolt.Factories
{
    public abstract class NodeFactory
    {
        public abstract INode Create(string name);
    }
}
