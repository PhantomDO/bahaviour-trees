﻿using Bolt;
using BTBolt;
using BTBolt.Tools;
using Ludiq;
using System;
using System.Collections.Generic;
using UnityEngine;

using BTBolt.Factories;
using System.Reflection;

namespace BTBolt
{
    public enum BTNodeState { RUNNING, SUCCESS, FAILURE, }

    [System.Serializable, IncludeInSettings(true)]
    public class BTNode<T> : INode
    {
        public bool IsRootNode { get; protected set; }
        public BTNodeState NodeState { get; protected set; }

        #region INode
        public virtual Type BaseType => GetType().BaseType;
        public Type RootType => typeof(BTNode<T>);
        public string Name { get; set; }
        public INode StartNode { get; set; }
        public List<INode> ConnectedNodes { get; set; }
        public virtual void SetConnectedNode(INode node) { }
        public virtual void OnInit()
        {
            IsRootNode = false;
            Name = this.GetType().Name;
            ConnectedNodes = new List<INode>();
        }
        public virtual BTNodeState OnEvaluate() { return NodeState; }
        public BTNode(string name = null)
        {
            OnInit();

            if (name != null)
                Name = name;
        }

        #endregion

        #region Factory
        public Dictionary<string, NodeFactory> Factories { get; private set; }

        public BTNode()
        {
            string factoryStr = "Factory";
            Factories = new Dictionary<string, NodeFactory>();
            var parentType = new[] { "Actions", "Conditionals", "Composites", "Decorators" };

            foreach (var folder in parentType)
            {
                var types = ScriptBuilder.GetAssembly("BTBoltFactoriesAssembly").GetTypesFromNamespace("BTBolt.Factories." + folder);
                //var types = typeof(NodeFactory).Assembly.GetStrTypesFromNamespace("BTBolt.Factories." + folder);
                foreach (var type in types)
                {
                    if (type != null)
                    {
                        var factory = (NodeFactory)Activator.CreateInstance(type);
                        var subType = type.Name.Substring(factoryStr.Length, type.Name.Length - factoryStr.Length);
                        Factories.Add(subType, factory);
                    }
                }
            }
        }

        public static BTNode<T> InitFactories() => new BTNode<T>();
        public INode ExecuteCreation(string type, string name = null) => Factories[type].Create(name);
        public void DebugFactories() { foreach (var item in Factories) Debug.Log($"Factory : {item.Key}, {item.Value}"); }        

        #endregion

        #region Reflected Unit

        public static T Node(INode node = null, string name = null)
        {
            var typeName = typeof(T).Name;
            var subStr = typeName.Substring(2, typeName.Length - 2);
            return BTNodeReflectedUnits.Node(node, subStr, name).To<T>();
        }

        #endregion
    }
}
