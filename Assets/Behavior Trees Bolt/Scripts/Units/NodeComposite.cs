﻿using Bolt;
using Ludiq;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using BTBolt.Tools;

namespace BTBolt.Units
{
    [UnitCategory("BehaviorTree/Node/Composites")]
    public abstract class NodeComposite<T> : NodeBase<T>
    {
        protected NodeComposite() { }

        [SerializeAs("outputCount")]
        protected int _outputCount = 1;

        [DoNotSerialize, Inspectable, InspectorLabel("Steps"), UnitHeaderInspectable("Steps")]
        public int OutputCount { get => _outputCount; set => _outputCount = Mathf.Clamp(value, 1, 10); }

        [DoNotSerialize]
        public List<ControlOutput> MultiOutputs { get; protected set; }

        public virtual void CopyFrom(NodeComposite<T> source)
        {
            base.CopyFrom(source);
            OutputCount = source.OutputCount;
        }

        protected override void Definition()
        {
            base.Definition();
            MultiOutputs = new List<ControlOutput>();
            for (int i = 0; i < OutputCount; i++)
            {
                ControlOutput output = base.ControlOutput(i.ToString());
                base.Succession(ControlEnter, output);
                MultiOutputs.Add(output);
            }
        }

        protected override void MultiOutputInvoke(Flow flow, GraphStack stack)
        {
            foreach (ControlOutput output in MultiOutputs)
            {
                flow.Invoke(output);
                flow.RestoreStack(stack);
            }
        }

        protected override IEnumerator MultiOutputInvokeCoroutine(Flow flow, GraphStack stack, NodeBase<T> sequence)
        {
            foreach (ControlOutput multiOutput in ((NodeComposite<T>)sequence).MultiOutputs)
            {
                yield return multiOutput;
                flow.RestoreStack(stack);
            }
        }
    }
}
