using BTBolt;
using BTBolt.Conditionals;
using Bolt;
using BTBolt.Units;

namespace BTBolt.Units.Conditionals
{
	[UnitCategory("BehaviorTree/Node/Conditionals")]
	public class NodeIs : NodeConditional<BTIs>  { }
}