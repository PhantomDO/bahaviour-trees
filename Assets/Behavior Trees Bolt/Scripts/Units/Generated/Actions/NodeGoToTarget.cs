using BTBolt;
using BTBolt.Actions;
using Bolt;
using BTBolt.Units;

namespace BTBolt.Units.Actions
{
	[UnitCategory("BehaviorTree/Node/Actions")]
	public class NodeGoToTarget : NodeAction<BTGoToTarget>  { }
}