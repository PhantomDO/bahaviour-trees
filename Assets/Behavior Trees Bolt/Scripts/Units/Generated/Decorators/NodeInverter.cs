using BTBolt;
using BTBolt.Decorators;
using Bolt;
using BTBolt.Units;

namespace BTBolt.Units.Decorators
{
	[UnitCategory("BehaviorTree/Node/Decorators")]
	public class NodeInverter : NodeDecorator<BTInverter>  { }
}