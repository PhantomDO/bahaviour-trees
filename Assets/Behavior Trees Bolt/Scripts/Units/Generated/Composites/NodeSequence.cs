using BTBolt;
using BTBolt.Composites;
using Bolt;
using BTBolt.Units;

namespace BTBolt.Units.Composites
{
	[UnitCategory("BehaviorTree/Node/Composites")]
	public class NodeSequence : NodeComposite<BTSequence>  { }
}