﻿using Bolt;
using BTBolt.Decorators;
using Ludiq;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using BTBolt.Tools;

namespace BTBolt.Units
{
    [UnitCategory("BehaviorTree/Node/Conditional")]
    public abstract class NodeConditional<T> : NodeBase<T>
    {

    }
}
