﻿using Bolt;
using Ludiq;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using BTBolt.Tools;
using BTBolt.Composites;

namespace BTBolt.Units
{
    [UnitCategory("BehaviorTree/Node/Composites")]
    public class NodeRoot : NodeComposite<BTRoot>
    {
        public NodeRoot() { }

        protected override void Definition()
        {
            ControlExit = base.ControlOutput("out");
            base.Definition();
        }

        public override BTRoot GetOutNode(INode sourceNode = null, string name = null)
        {
            if(sourceNode != null)
            {
                name = OutputName = sourceNode.Name;
                return sourceNode.To<BTRoot>();
            }

            return BTRoot.Node(sourceNode, name);
        }
    }
}
