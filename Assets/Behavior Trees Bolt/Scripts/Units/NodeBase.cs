using Bolt;
using Ludiq;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BTBolt.Factories;
using BTBolt.Tools;
using System.Reflection;

using System.CodeDom;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using Microsoft.VisualBasic;

namespace BTBolt.Units
{
    [UnitCategory("BehaviorTree/Node"), IncludeInSettings(true)]
    public abstract class NodeBase<T> : Unit
    {
        protected NodeBase() { }

        [DoNotSerialize]
        public Assembly Assembly => typeof(T).Assembly;

        [SerializeAs("outputName")]
        protected string _outputName = "";

        [DoNotSerialize, Inspectable, InspectorLabel("Name"), UnitHeaderInspectable("Name")]
        public string OutputName { get => _outputName; set => _outputName = value; }

        [DoNotSerialize]
        public ValueInput InNode { get; protected set; }

        [DoNotSerialize]
        public ValueOutput OutNode { get; protected set; }        

        [DoNotSerialize, PortLabelHidden]
        public ControlInput ControlEnter { get; protected set; }

        [DoNotSerialize, PortLabelHidden]
        public ControlOutput ControlExit { get; protected set; }

        [DoNotSerialize]
        public Dictionary<string, ValueInput> MembersInputs { get; protected set; }

        public virtual void CopyFrom(NodeBase<T> source)
        {
            base.CopyFrom(source);
            InNode = source.InNode;
            OutNode = source.OutNode;
            MembersInputs = source.MembersInputs;
            ControlExit = source.ControlExit;
        }

        protected override void Definition()
        {
            ControlEnter = base.ControlInputCoroutine("in", Enter, EnterCoroutine);
            InNode = base.ValueInput<INode>("In Node").AllowsNull();
            OutNode = base.ValueOutput<T>("Out Node");

            base.Requirement(InNode, ControlEnter);
            base.Assignment(ControlEnter, OutNode);
            if (ControlExit != null)
                base.Succession(ControlEnter, ControlExit);

            MembersInputs = new Dictionary<string, ValueInput>();

            foreach(var property in typeof(T).GetFields())
            {
                ValueInput input = base.ValueInput(property.FieldType, property.Name);
                input.SetDefaultValue(property.FieldType.Default());
                base.Requirement(input, OutNode);
                MembersInputs.Add(property.Name, input);
            }
        }

        public virtual T GetOutNode(INode sourceNode = null, string name = null)
        {
            var typeName = typeof(T).Name;
            var subStr = typeName.Substring(2, typeName.Length - 2);
            return BTNodeReflectedUnits.Node(sourceNode, subStr, name).To<T>();
        }

        protected T GetOutNode(Flow flow)
        {
            var fields = new Dictionary<string, object>();
            foreach (var member in MembersInputs)
            {
                fields.Add(member.Key, flow.GetValue(member.Value));
            }

            var outNode = GetOutNode(flow.GetValue<INode>(InNode), OutputName);
            outNode.SetFields(fields);            

            return outNode;
        }

        protected virtual void MultiOutputInvoke(Flow flow, GraphStack stack) { }
        protected virtual IEnumerator MultiOutputInvokeCoroutine(Flow flow, GraphStack stack, NodeBase<T> sequence) { yield return null; }

        protected virtual ControlOutput Enter(Flow flow)
        {
            var nodeValue = GetOutNode(flow);
            flow.SetValue(OutNode, nodeValue);

            GraphStack stack = flow.PreserveStack();

            MultiOutputInvoke(flow, stack);

            if (ControlExit != null) 
                flow.Invoke(ControlExit);

            flow.RestoreStack(stack);
            flow.DisposePreservedStack(stack);
            return null;
        }

        protected virtual IEnumerator EnterCoroutine(Flow flow)
        {
            var nodeValue = GetOutNode(flow);
            flow.SetValue(OutNode, nodeValue);

            NodeBase<T> sequence = null;
            GraphStack graphStack = flow.PreserveStack();

            MultiOutputInvokeCoroutine(flow, graphStack, sequence);

            if (ControlExit != null) 
                yield return sequence.ControlExit;

            flow.RestoreStack(graphStack);
            flow.DisposePreservedStack(graphStack);
        }
    }
}