﻿using Bolt;
using BTBolt.Decorators;
using Ludiq;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using BTBolt.Tools;

namespace BTBolt.Units
{
    [UnitCategory("BehaviorTree/Node/Decorators")]
    public abstract class NodeDecorator<T> : NodeBase<T>
    {
        protected override void Definition()
        {
            ControlExit = base.ControlOutput("out");
            base.Definition();
        }
    }
}
