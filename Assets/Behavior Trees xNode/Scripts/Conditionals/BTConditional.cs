﻿
namespace BTXNode.Conditionals
{
    [System.Serializable]
    public abstract class BTConditional : BTNode
    {
        protected override void Init()
        {
            SetType(BTNodeType.LEAF);
        }
    }
}
