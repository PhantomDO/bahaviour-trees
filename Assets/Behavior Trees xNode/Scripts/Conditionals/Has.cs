﻿
namespace BTXNode.Conditionals
{
    public enum HasType { S, F}

    [System.Serializable]
    public class Has : BTConditional
    {
        [Input] public HasType Type;
        public override BTNodeState Evaluate()
        {
            switch (Type)
            {
                case HasType.F:
                    NodeState = BTNodeState.FAILURE;
                    break;
                case HasType.S:
                    NodeState = BTNodeState.SUCCESS;
                    break;
                default:
                    break;
            }

            return NodeState;
        }
    }
}
