﻿
namespace BTXNode.Decorators
{
    [System.Serializable]
    public abstract class BTDecorator : BTNode
    {
        [Output] public BTNode Node;

        protected override void Init()
        {
            SetType(BTNodeType.BRANCH);
        }
    }
}
