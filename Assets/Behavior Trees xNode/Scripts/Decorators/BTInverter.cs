﻿
namespace BTXNode.Decorators
{
    [System.Serializable]
    public class BTInverter : BTDecorator
    {
        public override BTNodeState Evaluate()
        {
            switch (Node.Evaluate())
            {
                case BTNodeState.RUNNING:
                    NodeState = BTNodeState.RUNNING;
                    break;
                case BTNodeState.SUCCESS:
                    NodeState = BTNodeState.FAILURE;
                    break;
                case BTNodeState.FAILURE:
                    NodeState = BTNodeState.SUCCESS;
                    break;
                default:
                    break;
            }

            return NodeState;
        }
    }
}
