﻿using BTXNode.Composites;
using BTXNode.Tools;
using UnityEngine;
using XNode;

namespace BTXNode
{
    [System.Serializable]
    public class BTSceneGraph : SceneGraph
    {
        public BTRootNode RootNode
        {
            get
            {
                for (int i = 0; i < graph.nodes.Count; i++)
                {
                    BTNode node = (BTNode)graph.nodes[i];
                    if (node.IsTypeOf<BTRootNode>())
                        return node.ToRoot();
                }

                return null;
            }
        }

        public void DoUpdate()
        {
            RootNode.Evaluate();
            if (RootNode.NodeState == BTNodeState.FAILURE)
            {
                Debug.LogError($"The root node state is : {RootNode.NodeState}");
            }
        }
    }
}
