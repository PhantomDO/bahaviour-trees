﻿
using BTXNode.Actions;
using BTXNode.Composites;
using BTXNode.Tools;
using UnityEngine;
using XNode;

using System.Collections.Generic;
using System;

namespace BTXNode
{
    [System.Serializable, CreateAssetMenu]
    public class BTGraph : NodeGraph
    {
        public BTRootNode RootNode
        {
            get
            {
                for (int i = 0; i < nodes.Count; i++)
                {
                    BTNode node = (BTNode)nodes[i];
                    if (node.IsTypeOf<BTRootNode>())
                        return node.ToRoot();
                }

                return null;
            }
        }

        public void DoUpdate()
        {
            RootNode.Evaluate();
            if (RootNode.NodeState == BTNodeState.FAILURE)
            {
                Debug.LogError($"The root node state is : {RootNode.NodeState}");
            }
        }
    }
}
