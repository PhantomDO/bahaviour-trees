﻿using System.Collections.Generic;
using UnityEngine;

namespace BTXNode.Composites
{
    [System.Serializable]
    public class BTSelector : BTComposite
    {
        protected override void Init()
        {
            SetType(BTNodeType.BRANCH);
        }

        public override BTNodeState Evaluate()
        {
            UpdateListOrder();

            foreach (var node in Nodes)
            {
                Debug.Log($"[Type : {node.GetType()}, Order : {node.Order}] evaluate : {node.Value.name}");
                switch (node.Value.Evaluate())
                {
                    case BTNodeState.RUNNING:
                        NodeState = BTNodeState.RUNNING;
                        return NodeState;
                    case BTNodeState.SUCCESS:
                        NodeState = BTNodeState.SUCCESS;
                        return NodeState;
                    case BTNodeState.FAILURE:
                        break; 
                    default:
                        break;
                }
            }

            NodeState = BTNodeState.FAILURE;
            return NodeState;
        }
    }
}
