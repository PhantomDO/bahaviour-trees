﻿using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace BTXNode.Composites
{
    [System.Serializable]
    public class BTRootNode : BTComposite
    {
        protected override void Init()
        {
            SetType(BTNodeType.ROOT);
        }

        private void Reset()
        {
            name = "Root";
        }

        public override BTNodeState Evaluate()
        {
            UpdateListOrder();

            Debug.LogWarning($"BTRoot evaluate");

            foreach (var node in Nodes)
            {
                Debug.Log($"[{node.Order}] evaluate : {node.Value}");
                switch (node.Value.Evaluate())
                {
                    case BTNodeState.RUNNING:
                        NodeState = BTNodeState.RUNNING;
                        return NodeState;
                    case BTNodeState.SUCCESS:
                        NodeState = BTNodeState.SUCCESS;
                        return NodeState;
                    case BTNodeState.FAILURE:
                        break;
                    default:
                        break;
                }
            }

            NodeState = BTNodeState.FAILURE;
            return NodeState;
        }
    }
}
