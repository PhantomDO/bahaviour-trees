﻿using System.Collections.Generic;
using UnityEngine;

namespace BTXNode.Composites
{
    [System.Serializable]
    public class BTSequence : BTComposite
    {
        public override BTNodeState Evaluate()
        {
            bool isAnyNodeRunning = false;

            UpdateListOrder();
            foreach (var node in Nodes)
            {
                Debug.Log($"[Type : {node.GetType()}, Order : {node.Order}] evaluate : {node.Value.name}");
                switch (node.Value.Evaluate())
                {
                    case BTNodeState.RUNNING:
                        isAnyNodeRunning = true;
                        break;
                    case BTNodeState.SUCCESS:
                        break;
                    case BTNodeState.FAILURE:
                        NodeState = BTNodeState.FAILURE;
                        return NodeState;
                    default:
                        break;
                }
            }

            // If Success all the node in the list was a success
            NodeState = isAnyNodeRunning ? BTNodeState.RUNNING : BTNodeState.SUCCESS;
            return NodeState;
        }
    }
}
