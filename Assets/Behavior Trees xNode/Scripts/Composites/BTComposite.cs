﻿
using BTXNode.Tools;
using System.Collections.Generic;

namespace BTXNode.Composites
{
    [System.Serializable]
    public abstract class BTComposite : BTNode
    {
        [Output] public List<BTOrderedNode> Nodes = new List<BTOrderedNode>();

        protected override void Init()
        {
            SetType(BTNodeType.BRANCH);
        }

        protected void UpdateListOrder()
        {
            Nodes.Sort((x, y) => x.Order.CompareTo(y.Order));
        }

        public override void OnCreateConnection(BTNode from, BTNode to)
        {
            base.OnCreateConnection(from, to);

            if (from.IsTypeOf(out BTSequence sequenceNode))
            {
                sequenceNode.AddToLastIndex(to);
            }
            else if (from.IsTypeOf(out BTSelector selectorNode))
            {
                selectorNode.AddToLastIndex(to);
            }
            else if (from.IsTypeOf(out BTRootNode rootNode))
            {
                rootNode.AddToLastIndex(to);
            }

            UpdateListOrder();
        }

        public override void OnBeforeRemoveConnection(BTNode from, BTNode to)
        {
            base.OnCreateConnection(from, to);

            if (from.IsTypeOf(out BTSequence sequenceNode))
            {
                sequenceNode.RemoveAtIndex(to);
            }
            else if (from.IsTypeOf(out BTSelector selectorNode))
            {
                selectorNode.RemoveAtIndex(to);
            }
            else if (from.IsTypeOf(out BTRootNode rootNode))
            {
                rootNode.RemoveAtIndex(to);
            }

            UpdateListOrder();
        }
    }
}
