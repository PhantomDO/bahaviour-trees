﻿using BTXNode.Tools;

namespace BTXNode.Composites
{
    [System.Serializable]
    public class BTOrderedNode
    {
        public int Order;
        public BTNode Value;

        public BTOrderedNode(int order, BTNode node)
        {
            Order = order;
            Value = node;
        }

        public bool ContainValue(BTNode value)
        {
            return Value.Equals(value); 
        }

        public void UpdateIndex(int removedIndex = -1)
        {
            if (removedIndex == -1) return;
            else if (removedIndex >= 0 && Order > removedIndex)
            {
                Order--;
            }
        }
    }
}
