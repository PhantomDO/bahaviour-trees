﻿
using BTXNode.Tools;
using UnityEngine;
using XNode;

namespace BTXNode
{
    public enum BTNodeState { RUNNING, SUCCESS, FAILURE, }
    public enum BTNodeType { ROOT, BRANCH, LEAF, }

    [System.Serializable]
    public abstract class BTNode : Node
    {
        [Input] public BTNode StartNode;

        public override object GetValue(NodePort port)
        {
            if (port.fieldName == "result")
            {
                return GetInputValue<Node>(name, this);
            }
            else
            {
                return null;
            }
        }

        public override void OnCreateConnection(NodePort from, NodePort to)
        {
            OnCreateConnection((BTNode)from.node, (BTNode)to.node);
        }

        public virtual void OnCreateConnection(BTNode from, BTNode to)
        {
            if (!to.IsTypeOf<Composites.BTRootNode>())
                to.StartNode = from;
        }

        public override void OnBeforeRemoveConnection(NodePort from, NodePort to)
        {
            if (from.IsInput && from.IsConnected)
                ((BTNode)from.node).StartNode = null;
            OnBeforeRemoveConnection((BTNode)from.node, (BTNode)to.node);
        }
        public virtual void OnBeforeRemoveConnection(BTNode from, BTNode to) { }

        public BTNodeType NodeType { get; private set; }
        public BTNodeState NodeState { get; protected set; }

        public void SetType(BTNodeType type)
        {
            NodeType = type;
        }

        public abstract BTNodeState Evaluate();
    }
}
