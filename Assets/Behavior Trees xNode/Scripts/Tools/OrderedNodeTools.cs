﻿using BTXNode.Composites;
using System.Collections.Generic;

namespace BTXNode.Tools
{
    public static class OrderedNodeTools
    {
        public static bool ContainsValue(this List<BTOrderedNode> list, BTNode node)
        {
            foreach (var item in list)
            {
                if (item.ContainValue(node))
                    return true;
            }

            return false;
        }

        public static void UpdateIndexs(this List<BTOrderedNode> list, int removedIndex)
        {
            list.ForEach((BTOrderedNode n) => n.UpdateIndex(removedIndex));
        }

        public static int IndexOf(this List<BTOrderedNode> list, BTNode node)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].ContainValue(node))
                    return i;
            }

            return -1;
        }

        public static List<int> Keys(this List<BTOrderedNode> list)
        {
            List<int> keys = new List<int>();

            foreach (var item in list)
            {
                keys.Add(item.Order);
            }

            return keys;
        }
    }
}
