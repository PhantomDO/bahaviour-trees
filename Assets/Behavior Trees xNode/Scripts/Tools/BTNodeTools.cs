﻿using BTXNode.Actions;
using BTXNode.Composites;
using BTXNode.Conditionals;
using BTXNode.Decorators;
using System;
using UnityEngine;

namespace BTXNode.Tools
{
    public static class BTNodeTools
    {        
        public static bool IsTypeOf<T>(this BTNode node)
        {
            return (typeof(T).Name == node.GetType().Name);
        }

        public static bool IsTypeOf(this BTNode node, Type type)
        {
            return (type.Name == node.GetType().Name);
        }

        public static bool IsTypeOf(this BTNode node, string type)
        {
            return (type == node.GetType().Name);
        }

        public static bool IsTypeOf<T>(this Type type)
        {
            return (typeof(T).Name == type.Name);
        }

        public static bool IsTypeOf<T>(this BTNode node, out T outNode)
        {
            if (node.IsTypeOf<T>())
            {
                outNode = node.ToType<T>();
                return true; 
            }
            else
            {
                outNode = default(T);
                return false;
            }
        }

        public static T ToType<T>(this BTNode node)
        {
            var type = typeof(T);

            if (type.IsTypeOf<BTSelector>())
                return (T)(object)node.ToSelector();
            else if (type.IsTypeOf<BTSequence>())
                return (T)(object)node.ToSequence();
            else if (type.IsTypeOf<BTRootNode>())
                return (T)(object)node.ToRoot();
            else if (type.IsTypeOf<BTAction>())
                return (T)(object)node.ToAction();
            else if (type.IsTypeOf<BTDecorator>())
                return (T)(object)node.ToDecorator();
            else if (type.IsTypeOf<BTConditional>())
                return (T)(object)node.ToConditional();

            return default(T);
        }

        /// <summary>
        /// For selector and sequence
        /// </summary>
        /// <param name="node"></param>
        /// <param name="toAdd"></param>
        public static void AddToLastIndex(this BTComposite node, BTNode toAdd)
        {
            if (node == null)
            {
                Debug.LogError($"The node is null.");
                return;
            }
            else if (node != null && !node.Nodes.ContainsValue(toAdd))
            {
                int lastIndex = -1;
                var keys = node.Nodes.Keys();
                foreach(var key in keys)
                {
                    if (lastIndex < key)
                        lastIndex = key;
                }
                
                node.Nodes.Add(new BTOrderedNode(lastIndex + 1, toAdd));
            }
        }

        /// <summary>
        /// For selector and sequence
        /// </summary>
        /// <param name="node"></param>
        /// <param name="toRemove"></param>
        public static void RemoveAtIndex(this BTComposite node, BTNode toRemove)
        {
            if (node == null)
            {
                Debug.LogError($"The node is null.");
                return;
            }
            else if (node != null && node.Nodes.ContainsValue(toRemove))
            {
                var key = node.Nodes.IndexOf(toRemove);
                int index = node.Nodes[key].Order;

                if (key != -1)
                {
                    node.Nodes.RemoveAt(key);
                    node.Nodes.UpdateIndexs(index);
                }

                if (node.Nodes.Count <= 0) 
                    node.Nodes.Clear();
            }
        }

        public static BTSelector ToSelector(this BTNode node)
        {
            return node.IsTypeOf<BTSelector>() ? (BTSelector)node : null;
        }        
        public static BTSequence ToSequence(this BTNode node)
        {
            return node.IsTypeOf<BTSequence>() ? (BTSequence)node : null;
        }
        public static BTRootNode ToRoot(this BTNode node)
        {
            return node.IsTypeOf<BTRootNode>() ? (BTRootNode)node : null;
        }
        public static BTDecorator ToDecorator(this BTNode node)
        {
            return node.IsTypeOf<BTDecorator>() ? (BTDecorator)node : null;
        }
        public static BTAction ToAction(this BTNode node)
        {
            return node.IsTypeOf<BTAction>() ? (BTAction)node : null;
        }
        public static BTConditional ToConditional(this BTNode node)
        {
            return node.IsTypeOf<BTConditional>() ? (BTConditional)node : null;
        }
    }
}
