﻿
using UnityEngine;

namespace BTXNode.Actions
{
    [System.Serializable]
    public class GoToTarget : BTAction
    {
        [Input] public Transform Target;

        public override BTNodeState Evaluate()
        {
            return BTNodeState.FAILURE;
        }
    }
}
