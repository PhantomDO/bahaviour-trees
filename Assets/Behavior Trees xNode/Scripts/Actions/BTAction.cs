﻿
namespace BTXNode.Actions
{
    [System.Serializable]
    public abstract class BTAction : BTNode
    {
        protected override void Init()
        {
            SetType(BTNodeType.LEAF);
        }
    }
}
